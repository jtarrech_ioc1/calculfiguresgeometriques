package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

public class Cercle implements FiguraGeometrica{
    private final double radi;
    
    public Cercle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu el radi del cercle: ");
        this.radi = scanner.nextDouble();
    }
    
    @Override
    public double calcularArea() {
        return radi * radi;
    }
    
    @Override
    public double calcularPerimetre() {
        return 4 * radi;
    }
}
